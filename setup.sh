#!/bin/sh

if [ "$1" == "serve" ]; then
	bundle exec jekyll serve
elif [ "$1" == "build" ]; then
	JEKYLL_ENV=production bundle exec jekyll build
elif [ "$1" == "docker" ]; then
	JEKYLL_ENV=production bundle exec jekyll build
    docker stop landing
    docker run --name landing -v $(pwd)/_site:/usr/share/nginx/html:ro -d -p 80:80 -p 443:443 nginx
	#cp -r _site docker/
	#cd docker
	#docker build -t gitlab .
    #docker run -d -it gitlab
else
	echo "Unknown command. Run 'serve'"
fi
